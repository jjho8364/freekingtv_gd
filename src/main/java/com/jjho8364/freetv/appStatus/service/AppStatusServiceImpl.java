package com.jjho8364.freetv.appStatus.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jjho8364.freetv.appStatus.dao.AppStatusDAO;
import com.jjho8364.freetv.appStatus.dto.AppStatusDTO;

@Service
public class AppStatusServiceImpl implements AppStatusService {

	@Autowired
	AppStatusDAO appStatusDAO;
	
	@Override
	public AppStatusDTO selectAppStatus(HashMap<String, String> map) throws Exception {
		
		AppStatusDTO appStatusDTO = new AppStatusDTO();
		
		appStatusDTO = appStatusDAO.selectAppStatus(map);
		
		System.out.println("splmsg1 : " + appStatusDTO.getSplMsg1());
		
		return appStatusDTO;
	}

}
