package com.jjho8364.freetv.appStatus;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.JsonObject;
import com.jjho8364.freetv.appStatus.dto.AppStatusDTO;
import com.jjho8364.freetv.appStatus.service.AppStatusService;

@RestController
@RequestMapping("/status")
public class AppStatusController {

	@Autowired 
	private AppStatusService appStatusService;
	
	@ResponseBody
	@PostMapping("/appStatus")
	public String getAppStatus(@RequestBody HashMap<String, String> hashMap) {
		
		System.out.println("comein..");
		System.out.println("App name : " + hashMap.get("appNm"));
		System.out.println("App name : " + hashMap.get("appVs"));
		
		AppStatusDTO appStatusObj = new AppStatusDTO();
		JsonObject jsonObj = new JsonObject();
		
		try {
			appStatusObj = appStatusService.selectAppStatus(hashMap);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jsonObj.addProperty("appNm", appStatusObj.getAppNm());
			jsonObj.addProperty("appVs", appStatusObj.getAppVs());
			jsonObj.addProperty("appStatus", appStatusObj.getAppStatus());
			jsonObj.addProperty("adsStatus", appStatusObj.getAdsStatus());
			jsonObj.addProperty("splMsg1", appStatusObj.getSplMsg1());
			jsonObj.addProperty("splMsg2", appStatusObj.getSplMsg2());
			jsonObj.addProperty("splMsg3", appStatusObj.getSplMsg3());
			jsonObj.addProperty("nxtAppUrl", appStatusObj.getNxtAppUrl());
			jsonObj.addProperty("clsImgUrl", appStatusObj.getClsImgUrl());
			jsonObj.addProperty("mtImgUrl", appStatusObj.getMtImgUrl());
		}
		
		return jsonObj.toString();
	}
	
	@ResponseBody
	@GetMapping("/test")
	public String testController(@RequestParam(value = "appNm")String appNm) {
		
		System.out.println("appNm : " + appNm);
		
		return appNm;
	}
}
