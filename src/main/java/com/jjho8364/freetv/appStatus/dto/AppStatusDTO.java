package com.jjho8364.freetv.appStatus.dto;

public class AppStatusDTO {
	private String appNm;
	private String appVs;
	private String appStatus;
	private String adsStatus;
	private String splMsg1;
	private String splMsg2;
	private String splMsg3;
	private String nxtAppUrl;
	private String clsImgUrl;
	private String mtImgUrl;
	
	public AppStatusDTO() {
		
	}
	
	public AppStatusDTO(String appNm, String appVs, String appStatus, String adsStatus, String splMsg1, String splMsg2,
			String splMsg3, String nxtAppUrl, String clsImgUrl, String mtImgUrl) {
		super();
		this.appNm = appNm;
		this.appVs = appVs;
		this.appStatus = appStatus;
		this.adsStatus = adsStatus;
		this.splMsg1 = splMsg1;
		this.splMsg2 = splMsg2;
		this.splMsg3 = splMsg3;
		this.nxtAppUrl = nxtAppUrl;
		this.clsImgUrl = clsImgUrl;
		this.mtImgUrl = mtImgUrl;
	}
	
	public String getAppNm() {
		return appNm;
	}
	public void setAppNm(String appNm) {
		this.appNm = appNm;
	}
	public String getAppVs() {
		return appVs;
	}
	public void setAppVs(String appVs) {
		this.appVs = appVs;
	}
	public String getAppStatus() {
		return appStatus;
	}
	public void setAppStatus(String appStatus) {
		this.appStatus = appStatus;
	}
	public String getAdsStatus() {
		return adsStatus;
	}
	public void setAdsStatus(String adsStatus) {
		this.adsStatus = adsStatus;
	}
	public String getSplMsg1() {
		return splMsg1;
	}
	public void setSplMsg1(String splMsg1) {
		this.splMsg1 = splMsg1;
	}
	public String getSplMsg2() {
		return splMsg2;
	}
	public void setSplMsg2(String splMsg2) {
		this.splMsg2 = splMsg2;
	}
	public String getSplMsg3() {
		return splMsg3;
	}
	public void setSplMsg3(String splMsg3) {
		this.splMsg3 = splMsg3;
	}
	public String getNxtAppUrl() {
		return nxtAppUrl;
	}
	public void setNxtAppUrl(String nxtAppUrl) {
		this.nxtAppUrl = nxtAppUrl;
	}
	public String getClsImgUrl() {
		return clsImgUrl;
	}
	public void setClsImgUrl(String clsImgUrl) {
		this.clsImgUrl = clsImgUrl;
	}
	public String getMtImgUrl() {
		return mtImgUrl;
	}
	public void setMtImgUrl(String mtImgUrl) {
		this.mtImgUrl = mtImgUrl;
	}
	
	
}
